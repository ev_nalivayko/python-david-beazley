# -*- coding: utf-8 -*-


# Программы вычисляет сложные проценты

# Количество лет
numyears = 5
# Процент
percent = 6.4
# Сумма вклада
deposit = 10000
# Расчет:
year = 1
while year <= numyears:
    deposit = deposit * (1 + percent / 100)
    # Вывод без форматирования
    print(year, deposit)
    # Вывод с форматированием стандартным методом
    print("%3d %0.1f" % (year, deposit))
    # Вывод с форматированием функцией format()
    print(format(year, "4d"), format(deposit, "0.2f"))
    # Вывод с форматированием методом .format()
    print("{0:5d} {1:0.3f}".format(year, deposit))
    year += 1