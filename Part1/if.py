# -*- coding: utf-8 -*-

# Условные операторы для простых проверок

a = 5
b = 8

# Простая проверка:
if a < b:
    print("A < B")
else:
    print("A > B")

# Провека нескольких вариантов
if a < b:
    print("A < B")
# elif  может быть сколько угодно
elif a == b:
    print("A = B")
else:
    print("A > B")

# else  может отсутствоват, программа пойдет дальше после проверки условия и выполнения или не выполнения тела
if a < b:
    print("A < B")
print("Hello!")

# Чтобы ничего не делать при определенном условии, нужно добавить команду pass
if a < b:
    print("A < B")
# elif  может быть сколько угодно
elif a == b:
    pass
else:
    print("A > B")

# Можно формировать булевы значения при помощи слов or, and, not.
# А сами булевы записывабтся с большой буквы True, False
if a < b and a != 3 and not b == 1:
    print("A < B and a!=3 and not b=!1")
elif a == 3 or b == 70:
    print("a == 3 or b == 70")
else:
    print("A > B")

# Перенос строки в соответствии с PEP8 делается как пробел и обратный слеш " \" при этом следующая строка может
    # иметь любой отступ
if a < b and a != 3 and not b == 1 and not b == 1 and not b == 3 and not b == 5 and not b == 6 and not b == 77 and \
        not b == 66 and not b == 11:
    print("Так вот")
elif a == 3 or b == 70:
    print("a == 3 or b == 70")
else:
    print("A > B")

# Оператор вхождения in, может вернуть True или False
slovo = "slovo"
chek = "ovo" in slovo
print(chek)