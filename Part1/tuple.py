# -*- coding: utf-8 -*-
# Кортежи
# Кортежи подчиняются правилам списков, но элементы кортежа нельзя менять.
# Поэтому кортеж используется для создания структуры данных

# Создание кортежа
t = ("a",)
b = "b",
c = ()
d = ("a",2,b)
print(b)
print(t)
print(c)
print(d)

# Кортеж можно разложить по переменным
tuple1 = ("Evgeny", "Nalivayko", "26")
print(tuple1)
firstname , lastname, age = tuple1
print("Имя: " + firstname)
print("Фамилия: " + lastname)
print("Возраст: " + age)

# К элементам кортежа можно обращаться через индекс, жобавлять новые элементы нельзя
print(tuple1[0])

# Списки и кортежи используют совместно Например работа с файлом csv -  преобразуем значения из файла в кортеж
file_csv = open("price_book.csv")
list1 = []
for line in file_csv:
    # Режем каждую строку на элементы по запятой и получаем список для каждой строки
    string1 = line.split(",")
    name = string1[0]
    count = int(string1[1])
    price = int(string1[2])
    # Создаем кортеж
    corteg = (name, count, price)
    # Добавляем кортеж в список
    list1.append(corteg)
print(list1)
# Получим один элемент списка
print(list1[0])
# Получим один элемент из кортежа, который в списке
print(list1[0][2])

# Распечатка значение кортежа из списка кортежей и их использование
total = 0
# Распечатываем
for name, count, price in list1:
    # Испльзуем
    total += count*price
print(total)




